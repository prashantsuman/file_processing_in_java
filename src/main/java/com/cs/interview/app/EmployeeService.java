package com.cs.interview.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class EmployeeService {
    private static final Logger log = LoggerFactory.getLogger(Application.class);
    @Autowired
    private EmployeeRepository employeeRepository;

    public void readDataFromFile() {
        System.out.println("Reading Data from File");
        String fileName = "C:\\ps\\workarea\\java_workarea\\CSInterview\\src\\main\\resources\\InputFile_20180430_120000.txt";
        try {
            List<String> stream = Files.readAllLines(Paths.get(fileName));
            String header = stream.get(0).replaceFirst("#", "");

            log.info("Header : " + header);
            Integer records = Integer.parseInt(stream.get(stream.size() - 1).replaceFirst("#RECORD COUNT ", ""));
            log.info("Record Count : " + records);

            if (records != stream.size() - 2) {
                log.error("Invaid row count in file " + fileName);
                throw new RuntimeException("Invaid row count in file " + fileName);
            }
            // stream.parallelStream().filter(line -> !line.startsWith("#")).map(line -> Arrays.asList(line.split("\\|"))).collect(Collector.of());
            //).stream().map(x-> x.trim())).forEach(x->System.out.println(x));


            stream.parallelStream().filter(line -> !line.startsWith("#"))
                    .map(line -> {


                                Employee employee = null;
                                String[] p = line.split("\\|");// a CSV has comma separated lines
                                if (p.length < 3)
                                    throw new RuntimeException("Invalid data in the file.");
                                else if (p.length >= 2) {
                                    if (p[0].trim().length() == 0 || p[2].trim().length() == 0)
                                        throw new RuntimeException("Invalid Id/Name in the file.");
                                    employee = new Employee();
                                    employee.setId(Long.parseLong(p[0].trim()));
                                    if (p[1] != null && p[1].trim().length() > 0)
                                        employee.setDepartment(p[1].trim());
                                    employee.setName(p[2].trim());

                                    if (p.length == 4)
                                        try {
                                            employee.setHiringDate(new SimpleDateFormat("dd/MM/yyyy").parse(p[3]));
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                    employee.setComments(new Date().toString());
                                }

                                return employee;
                            }
                    )
                    .forEach(employee -> employeeRepository.save(employee));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void storeRecordsInDatabase() {
        System.out.println("Storing Data into DB");
        //employeeRepository.save(new Employee(1L,"Prashant","CS",new Date(),"Hi"));
        // fetch all customers
        log.info("Customers found with findAll():");
        log.info("-------------------------------");
        for (Employee employee : employeeRepository.findAll()) {
            log.info(employee.toString());
        }
    }
}
/*
@FunctionalInterface
private Function<String, Employee> mapToItem = (line) -> {
        String[] p = line.split("|");// a CSV has comma separated lines
        Employee item = new Employee();
        item.setId(Long.parseLong(p[0]));//<-- this is the first column in the csv file
        */
/*if (p[3] != null && p[3].trim().length() > 0) {
            item.setSomeProeprty(p[3]);
        }*//*

        //more initialization goes here
        return item;
*/
//}
