package com.cs.interview.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@ComponentScan("com.cs.interview.app")
public class Application implements CommandLineRunner {
    private static final Logger log = LoggerFactory.getLogger(Application.class);
    @Autowired
    private EmployeeService employeeService;

    public static void main(String[] args) {
        SpringApplication.run(Application.class);
    }

    @Override
    public void run(String... args) {
        employeeService.readDataFromFile();
        employeeService.storeRecordsInDatabase();
    }
/*

    @Bean
    public CommandLineRunner demo(EmployeeRepository repository) {
        return (args) -> {
            // save a couple of customers
            repository.save(new Employee("Jack", "Bauer"));
            repository.save(new Employee("Chloe", "O'Brian"));
            repository.save(new Employee("Kim", "Bauer"));
            repository.save(new Employee("David", "Palmer"));
            repository.save(new Employee("Michelle", "Dessler"));

            // fetch all customers
            log.info("Customers found with findAll():");
            log.info("-------------------------------");
            for (Employee employee : repository.findAll()) {
                log.info(employee.toString());
            }
            log.info("");

            //fetch an individual customer by ID
			repository.findById(1L)
				.ifPresent(employee -> {
					log.info("Employee found with findById(1L):");
					log.info("--------------------------------");
					log.info(employee.toString());
					log.info("");
				});

            // fetch customers by last name
            log.info("Employee found with findByLastName('Bauer'):");
            log.info("--------------------------------------------");
            repository.findByLastName("Bauer").forEach(bauer -> {
                log.info(bauer.toString());
            });
            // for (Employee bauer : repository.findByLastName("Bauer")) {
            // 	log.info(bauer.toString());
            // }
            log.info("");
        };
    }
*/

}
